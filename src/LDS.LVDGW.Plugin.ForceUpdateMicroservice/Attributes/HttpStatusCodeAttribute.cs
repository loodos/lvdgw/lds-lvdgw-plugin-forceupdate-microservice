﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace LDS.LVDGW.Plugin.ForceUpdateMicroservice.Attributes
{
    public class HttpStatusCodeAttribute : Attribute
    {
        public HttpStatusCode StatusCode { get; private set; }

        internal HttpStatusCodeAttribute(HttpStatusCode statusCode)
        {
            StatusCode = statusCode;
        }
    }
}
