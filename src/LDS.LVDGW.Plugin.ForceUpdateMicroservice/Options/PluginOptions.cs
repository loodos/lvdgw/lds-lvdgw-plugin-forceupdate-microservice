﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LDS.LVDGW.Plugin.ForceUpdateMicroservice.Options
{
    public class PluginOptions
    {
        public enum ContainerType
        {
            Header,
            Form,
            Query
        }

        public string RulesFilePath { get; set; }
        public string Container { get; set; }
        public string ContainerKey_Platform { get; set; }
        public string ContainerKey_PlatformVersion { get; set; }
        public string ContainerKey_AppVersion { get; set; }
        public string StoreUrl_Android { get; set; }
        public string StoreUrl_iOS { get; set; }
    }
}
