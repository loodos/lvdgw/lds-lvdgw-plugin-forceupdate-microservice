﻿using System;
using System.Net;
using System.Threading.Tasks;
using LDS.LVDGW.Plugin.Microservice;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using LDS.LVDGW.Plugin.ForceUpdateMicroservice.Services;
using LDS.LVDGW.Plugin.ForceUpdateMicroservice.Options;
using LDS.LVDGW.Plugin.ForceUpdateMicroservice.Services.Interfaces;
using LDS.LVDGW.Plugin.ForceUpdateMicroservice.Models;

namespace LDS.LVDGW.Plugin.ForceUpdateMicroservice
{
    public class Plugin : PluginBase, IMicroservicePlugin
    {
        //public override string Name => "ForceUpdateMicroservice";

        [ParameterInfo("RulesFilePath", "Rules file path", Mandatory = true, ParameterType = ParameterTypes.Text, SampleValue = "/rules.json")]
        public string RulesFilePath { get; set; }

        [ParameterInfo("Container", "Parameter containers", Mandatory = true, ParameterType = ParameterTypes.Text, SampleValue = "Header", DefaultValue = "Header")]
        public string Container { get; set; }

        [ParameterInfo("ContainerKey_Platform", "Container Key for Platform", Mandatory = true, ParameterType = ParameterTypes.Text, SampleValue = "X-APP-Platform", DefaultValue = "X-APP-Platform")]
        public string ContainerKey_Platform { get; set; }

        [ParameterInfo("ContainerKey_PlatformVersion", "Container Key for Platform Version", Mandatory = true, ParameterType = ParameterTypes.Text, SampleValue = "X-APP-PlatformVersion", DefaultValue = "X-APP-PlatformVersion")]
        public string ContainerKey_PlatformVersion { get; set; }

        [ParameterInfo("ContainerKey_AppVersion", "Container Key for App Version", Mandatory = true, ParameterType = ParameterTypes.Text, SampleValue = "X-APP-AppVersion", DefaultValue = "X-APP-AppVersion")]
        public string ContainerKey_AppVersion { get; set; }

        [ParameterInfo("StoreUrl_Android", "Store Url for Android", Mandatory = true, ParameterType = ParameterTypes.Text, SampleValue = "http://play.google.com/store/apps/details?id=<package_name>", DefaultValue = "http://play.google.com/store/apps/details?id=<package_name>")]
        public string StoreUrl_Android { get; set; }

        [ParameterInfo("StoreUrl_iOS", "Store Url for iOS", Mandatory = true, ParameterType = ParameterTypes.Text, SampleValue = "https://itunes.apple.com/app/id<app_id>", DefaultValue = "https://itunes.apple.com/app/id<app_id>")]
        public string StoreUrl_iOS { get; set; }

        public override void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddSingleton<IRuleService, RuleService>();
            services.AddScoped<IHttpParametersService, HttpParametersService>();

            services.Configure<PluginOptions>(options => {
                options.RulesFilePath = RulesFilePath;
                options.Container = Container;
                options.ContainerKey_Platform = ContainerKey_Platform;
                options.ContainerKey_PlatformVersion = ContainerKey_PlatformVersion;
                options.ContainerKey_AppVersion = ContainerKey_AppVersion;
                options.StoreUrl_Android = StoreUrl_Android;
                options.StoreUrl_iOS = StoreUrl_iOS;
            });
        }

        public async Task<MicroserviceResponse> ExecAsync(HttpRequest httpRequest)
        {
            try
            {
                using (var serviceScope = httpRequest.HttpContext.RequestServices.GetService<IServiceScopeFactory>().CreateScope())
                {
                    var httpParametersService = serviceScope.ServiceProvider.GetRequiredService<IHttpParametersService>();
                    var ruleService = serviceScope.ServiceProvider.GetRequiredService<IRuleService>();

                    return await ruleService.GetResponse(httpParametersService);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "ForceUpdateMicroservice generic exception occured.");

                return ResponseModel<object>.Create(ex.Message, ProcessStatusCodes.UnhandledException)
                    .ToMicroserviceResponse();
            }
        }
    }
}
