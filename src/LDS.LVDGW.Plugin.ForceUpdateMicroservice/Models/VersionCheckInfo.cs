﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace LDS.LVDGW.Plugin.ForceUpdateMicroservice.Models
{
    public class VersionCheckInfo
    {
        public Version Version { get; set; }
        public string Operator { get; set; } // "*,<=,<,==,=" Default: <=

        public bool Check(Version versionToCheck)
        {
            if (versionToCheck > Version.Parse("0.0.0.0"))
            {
                if (Operator == "==" || Operator == "=")
                {
                    if (versionToCheck == Version)
                        return true;
                }
                else if (Operator == "*")
                {
                    return true;
                }
                else if (Operator == "<")
                {
                    if (versionToCheck < Version)
                        return true;
                }
                else //if (Operator == "<=")
                {
                    if (versionToCheck <= Version)
                        return true;
                }
            }

            return false;
        }
    }
}
