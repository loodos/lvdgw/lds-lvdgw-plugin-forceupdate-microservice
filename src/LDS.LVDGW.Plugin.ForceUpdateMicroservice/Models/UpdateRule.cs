﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace LDS.LVDGW.Plugin.ForceUpdateMicroservice.Models
{
    public class UpdateRule
    {
        public enum UpdateTypes
        {
            NoUpdate,
            SoftUpdate,
            MandatoryUpdate
        }

        public string Platform { get; set; }
        public VersionCheckInfo PlatformVersion { get; set; }
        public VersionCheckInfo AppVersion { get; set; }
        public UpdateTypes UpdateType { get; set; }
        public string LocalizationKeyPrefix { get; set; }
    }
}
