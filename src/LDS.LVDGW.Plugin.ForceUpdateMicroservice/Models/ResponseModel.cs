﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Runtime.Serialization;
using System.Text;
using LDS.LVDGW.Plugin.ForceUpdateMicroservice.Attributes;
using LDS.LVDGW.Plugin.ForceUpdateMicroservice.Extensions;
using LDS.LVDGW.Plugin.Microservice;
using Newtonsoft.Json;

namespace LDS.LVDGW.Plugin.ForceUpdateMicroservice.Models
{
    public enum ProcessStatusCodes
    {
        [HttpStatusCode(HttpStatusCode.OK)]
        Success = 2100,

        [HttpStatusCode(HttpStatusCode.OK)]
        NoUpdate,
        [HttpStatusCode(HttpStatusCode.OK)]
        SoftUpdate,
        [HttpStatusCode(HttpStatusCode.OK)]
        MandatoryUpdate,

        [HttpStatusCode(HttpStatusCode.InternalServerError)]
        UnhandledException = 2999,
    }

    [DataContract]
    public class ResponseModel<T>
    {
        [DataMember]
        public T Data { get; set; }

        [DataMember]
        public ProcessStatusModel ProcessStatus { get; set; }

        [DataMember]
        public FriendlyMessageModel FriendlyMessage { get; set; }


        public static ResponseModel<object> Create(ProcessStatusCodes processStatus = ProcessStatusCodes.Success, FriendlyMessageModel friendlyMessage = null)
        {
            return ResponseModel<object>.Create(null, processStatus, friendlyMessage); //string.Empty->null olarak değiştirildi data:"" yerine data=null dönmesi gerekiyor 
        }
        public static ResponseModel<object> Create(T data, ProcessStatusCodes processStatus = ProcessStatusCodes.Success, FriendlyMessageModel friendlyMessage = null)
        {
            var resultObject = new { response = data, message=(object) null };
            var response = new ResponseModel<object>
            {
                Data = resultObject,
                ProcessStatus = ProcessStatusModel.Create(processStatus),
                FriendlyMessage = friendlyMessage
            };

            return response;
        }

        public static ResponseModel<Exception> CreateException(Exception ex, ProcessStatusCodes processStatus, FriendlyMessageModel friendlyMessage)
        {
            var response = new ResponseModel<Exception>()
            {
                Data = ex,
                ProcessStatus = ProcessStatusModel.Create(processStatus),
                FriendlyMessage = friendlyMessage
            };

            return response;
        }

        public MicroserviceResponse ToMicroserviceResponse()
        {
            return new MicroserviceResponse()
            {
                ContentType = "application/json",
                StatusCode = (int)ProcessStatus.ProcessStatusCode.GetAttribute<HttpStatusCodeAttribute>().StatusCode,
                Body = JsonConvert.SerializeObject(this)
            };
        }
    }

    [DataContract]
    public class ProcessStatusModel
    {
        [IgnoreDataMember]
        public ProcessStatusCodes ProcessStatusCode { get; set; }

        [DataMember(Name = "Code")]
        public int ProcessStatusCodeInt
        {
            get { return (int)ProcessStatusCode; }
            set { ProcessStatusCode = (ProcessStatusCodes)value; }
        }

        [DataMember]
        public string Description => ProcessStatusCode.ToString();

        [DataMember]
        public bool Success => ProcessStatusCode.GetAttribute<HttpStatusCodeAttribute>().StatusCode == HttpStatusCode.OK;

        public static ProcessStatusModel Create(ProcessStatusCodes processStatusCode)
        {
            return new ProcessStatusModel() { ProcessStatusCode = processStatusCode };
        }
    }

    [DataContract]
    public class FriendlyMessageModel
    {
        public enum DisplayTypes
        {
            POPUP,
            TOAST
        }

        [IgnoreDataMember]
        public DisplayTypes DisplayType { get; set; } = DisplayTypes.POPUP;


        [DataMember(Name = "DisplayType")]
        public string DisplayTypeString => DisplayType.ToString();

        [DataMember]
        public string Title { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public bool Cancelable { get; set; } = true;

        [DataMember]
        public List<FriendlyMessageButtonModel> ButtonSet { get; set; } = new List<FriendlyMessageButtonModel>();

        public static FriendlyMessageModel Create(string title, string description, string buttonPositiveText = null, string buttonNegativeText = null, string buttonNeutralText = null)
        {
            var friendlyMessageModel = new FriendlyMessageModel()
            {
                Title = title,
                Description = description
            };

            if (!string.IsNullOrWhiteSpace(buttonPositiveText))
            {
                friendlyMessageModel.ButtonSet.Add(FriendlyMessageButtonModel.Create(buttonPositiveText));
            }
            if (!string.IsNullOrWhiteSpace(buttonNegativeText))
            {
                friendlyMessageModel.ButtonSet.Add(FriendlyMessageButtonModel.Create(buttonPositiveText, FriendlyMessageButtonModel.ButtonTypes.NEGATIVE));
            }
            if (!string.IsNullOrWhiteSpace(buttonNeutralText))
            {
                friendlyMessageModel.ButtonSet.Add(FriendlyMessageButtonModel.Create(buttonPositiveText, FriendlyMessageButtonModel.ButtonTypes.NEUTRAL));
            }

            return friendlyMessageModel;
        }
    }

    [DataContract]
    public class FriendlyMessageButtonModel
    {
        public enum ButtonTypes
        {
            POSITIVE,
            NEGATIVE,
            NEUTRAL
        }

        public enum ButtonActions
        {
            DEEPLINK,
            CLOSE,
            URL,
            FORCE_UPDATE,
            SEESION_TIMEOUT
        }

        [IgnoreDataMember]
        public ButtonTypes ButtonType { get; set; } = ButtonTypes.POSITIVE;

        [IgnoreDataMember]
        public ButtonActions ButtonAction { get; set; } = ButtonActions.CLOSE;


        [DataMember(Name = "Type")]
        public string ButtonTypeString => ButtonType.ToString();

        [DataMember]
        public string Text { get; set; }

        [DataMember(Name = "Action")]
        public string ButtonActionString => ButtonAction.ToString();

        [DataMember]
        public string ActionParameter { get; set; }

        public static FriendlyMessageButtonModel Create(string text, ButtonTypes type = ButtonTypes.POSITIVE, ButtonActions action = ButtonActions.CLOSE, string actionParameter = null)
        {
            var button = new FriendlyMessageButtonModel()
            {
                Text = text,
                ButtonType = type,
                ButtonAction = action,
                ActionParameter = actionParameter
            };

            return button;
        }

    }
}
