﻿using System;
using System.Linq;
using LDS.LVDGW.Plugin.ForceUpdateMicroservice.Options;
using LDS.LVDGW.Plugin.ForceUpdateMicroservice.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Primitives;

namespace LDS.LVDGW.Plugin.ForceUpdateMicroservice.Services
{
    public class HttpParametersService : IHttpParametersService
    {
        private readonly PluginOptions _pluginOptions;
        private readonly IHttpContextAccessor _httpContextAccessor;

        private readonly HttpRequest _request;

        public string Platform { get; set; }
        public Version PlatformVersion { get; set; }
        public Version AppVersion { get; set; }

        public HttpParametersService(IHttpContextAccessor httpContextAccessor, IOptions<PluginOptions> pluginOptions)
        {
            _httpContextAccessor = httpContextAccessor;
            _request = _httpContextAccessor.HttpContext.Request;
            _pluginOptions = pluginOptions.Value;

            Platform = GetValue(_pluginOptions.ContainerKey_Platform);

            var platformVersion = GetValue(_pluginOptions.ContainerKey_PlatformVersion);
            PlatformVersion = int.TryParse(platformVersion, out var platformVer) ? new Version(platformVer, 0, 0, 0) :
                Version.TryParse(platformVersion, out var ver) ? ver : new Version(0, 0, 0, 0);

            var appVersion = GetValue(_pluginOptions.ContainerKey_AppVersion);
            AppVersion = int.TryParse(appVersion, out var appVer) ? new Version(appVer, 0, 0, 0) :
                Version.TryParse(appVersion, out var version) ? version : new Version(0, 0, 0, 0);

        }

        private string GetValue(string key)
        {
            var values = StringValues.Empty;

            var container = Enum.Parse<PluginOptions.ContainerType>(_pluginOptions.Container);

            if (container == PluginOptions.ContainerType.Header)
            {
                _request.Headers.TryGetValue(key, out values);
            }
            else if (container == PluginOptions.ContainerType.Form && _request.HasFormContentType)
            {
                _request.Form.TryGetValue(key, out values);
            }
            else if (container == PluginOptions.ContainerType.Query)
            {
                _request.Query.TryGetValue(key, out values);
            }

            return values.FirstOrDefault();
        }
    }
}
