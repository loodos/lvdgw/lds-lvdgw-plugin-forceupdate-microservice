﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LDS.LVDGW.Plugin.ForceUpdateMicroservice.Services.Interfaces
{
    public interface IHttpParametersService
    {
        string Platform { get; set; }
        Version PlatformVersion { get; set; }
        Version AppVersion { get; set; }
    }
}
