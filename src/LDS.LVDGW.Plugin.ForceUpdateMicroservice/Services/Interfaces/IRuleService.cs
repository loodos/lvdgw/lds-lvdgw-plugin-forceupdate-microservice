﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using LDS.LVDGW.Plugin.Microservice;

namespace LDS.LVDGW.Plugin.ForceUpdateMicroservice.Services.Interfaces
{
    public interface IRuleService
    {
        Task<MicroserviceResponse> GetResponse(IHttpParametersService parameters);
    }
}
