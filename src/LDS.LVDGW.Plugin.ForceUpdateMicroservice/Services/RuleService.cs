﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using LDS.LVDGW.Plugin.ForceUpdateMicroservice.Models;
using LDS.LVDGW.Plugin.ForceUpdateMicroservice.Options;
using LDS.LVDGW.Plugin.ForceUpdateMicroservice.Services.Interfaces;
using LDS.LVDGW.Plugin.Microservice;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Serilog;

namespace LDS.LVDGW.Plugin.ForceUpdateMicroservice.Services
{
    public class RuleService : IRuleService
    {
        private readonly PluginOptions _pluginOptions;
        private readonly IStringLocalizer<Plugin> _localizer;

        private List<UpdateRule> _rules = new List<UpdateRule>();
        private DateTime _lastWriteTime = DateTime.MinValue;

        public RuleService(IOptions<PluginOptions> pluginOptions, IStringLocalizer<Plugin> localizer)
        {
            _pluginOptions = pluginOptions.Value;
            _localizer = localizer;
        }

        private async Task<List<UpdateRule>> ReadRulesFileAsync()
        {
            List<UpdateRule> rules = null;

            // read configuration file
            using (var reader = File.OpenText(_pluginOptions.RulesFilePath))
            {
                var jsonRules = await reader.ReadToEndAsync();

                try
                {
                    rules = JsonConvert.DeserializeObject<List<UpdateRule>>(jsonRules);
                }
                catch (Exception ex)
                {
                    rules = null;
                    Log.Error(ex, "Force update rules file invalid!");
                }
            }

            return rules;
        }

        private async Task<List<UpdateRule>> GetRulesAsync()
        {
            if (File.Exists(_pluginOptions.RulesFilePath))
            {
                var lastWriteTime = File.GetLastWriteTimeUtc(_pluginOptions.RulesFilePath);

                // check configuration files changed
                if (lastWriteTime > _lastWriteTime)
                {
                    var newRules = await ReadRulesFileAsync();

                    if (newRules != null)
                        _rules = newRules;

                    _lastWriteTime = lastWriteTime;
                }
            }

            return _rules;
        }

        private async Task<UpdateRule> FindMatchedRuleAsync(IHttpParametersService parameters)
        {
            var rules = await GetRulesAsync();

            // find matched rule in update rules
            var platformRules = rules.Where(x => x.Platform == "*" || x.Platform.Equals(parameters.Platform, StringComparison.InvariantCultureIgnoreCase));

            var matchedRule = new UpdateRule() { UpdateType = UpdateRule.UpdateTypes.NoUpdate };

            foreach (var platformRule in platformRules.Reverse().ToList())
            {
                if (platformRule.PlatformVersion.Check(parameters.PlatformVersion))
                {
                    if (platformRule.AppVersion.Check(parameters.AppVersion))
                    {
                        matchedRule = platformRule;
                        break;
                    }
                }
            }

            return matchedRule;
        }

        public async Task<MicroserviceResponse> GetResponse(IHttpParametersService parameters)
        {
            var rule = await FindMatchedRuleAsync(parameters);

            // no update
            if (rule.UpdateType == UpdateRule.UpdateTypes.NoUpdate)
            {
                return ResponseModel<object>.Create(ProcessStatusCodes.NoUpdate)
                    .ToMicroserviceResponse();
            }
            // mandatory or soft update
            else
            {
                var storeUrl = rule.Platform.Equals("iOS", StringComparison.InvariantCultureIgnoreCase)
                    ? _pluginOptions.StoreUrl_iOS
                    : _pluginOptions.StoreUrl_Android;

                var processStatusCode = rule.UpdateType == UpdateRule.UpdateTypes.SoftUpdate
                        ? ProcessStatusCodes.SoftUpdate
                        : ProcessStatusCodes.MandatoryUpdate;

                var friendlyMessage = FriendlyMessageModel.Create(
                    _localizer[$"{rule.LocalizationKeyPrefix}_Message_Title"],
                    _localizer[$"{rule.LocalizationKeyPrefix}_Message_Description"]
                );

                // add positive button. (soft or mandatory)
                friendlyMessage.ButtonSet.Add(FriendlyMessageButtonModel.Create(
                        _localizer[$"{rule.LocalizationKeyPrefix}_Message_ButtonPositiveText"],
                        FriendlyMessageButtonModel.ButtonTypes.POSITIVE,
                        FriendlyMessageButtonModel.ButtonActions.FORCE_UPDATE,
                        storeUrl
                    ));

                if (rule.UpdateType == UpdateRule.UpdateTypes.MandatoryUpdate)
                {
                    // mandatory update not cancelable
                    friendlyMessage.Cancelable = false;
                }
                else if (rule.UpdateType == UpdateRule.UpdateTypes.SoftUpdate)
                {
                    // add skip update button for soft update
                    friendlyMessage.ButtonSet.Add(FriendlyMessageButtonModel.Create(
                        _localizer[$"{rule.LocalizationKeyPrefix}_Message_ButtonNegativeText"],
                        FriendlyMessageButtonModel.ButtonTypes.NEGATIVE,
                        FriendlyMessageButtonModel.ButtonActions.CLOSE
                    ));
                }

                return ResponseModel<object>.Create(processStatusCode, friendlyMessage)
                    .ToMicroserviceResponse();
            }
        }
    }
}
